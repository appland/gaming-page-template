
Games Club Website Template
======
This template is free to use for you, our partner, and is supposed to support you in building an attractive website for your end customers. It contains an integration against the Games Feed API to allow you to display all the games and apps that exist in your club. On [https://docs.appland.se/landing-pages/games-club/](https://docs.appland.se/landing-pages/games-club/) you find the latest version of the template. Please read the instructions below on how to integrate this template to your solution.

## Versions
In the repository you find the versions under git tags.

**Version 2.1** - *2018-11-08*  
*Improved loading time of the applications. Added possibility for adding multiple GA trackers.*  
[https://docs.appland.se/landing-pages/games-club-version-2.1/](https://docs.appland.se/landing-pages/games-club-version-2.1/)
 
**Version 2.0** - *2018-05-29*  
*A more complete web site with more sub pages. Added in this version are "About Us", "Pricing", "FAQ", "Contact Us", "Privacy Policy" and "Terms and Conditions"*  
[https://docs.appland.se/landing-pages/games-club-version-2.0/](https://docs.appland.se/landing-pages/games-club-version-2.0/)

**Version 1.0** - *2018-02-16*  
*First version that contains a landing page and a page for showing all games.*  
[https://docs.appland.se/landing-pages/games-club-version-1.0/](https://docs.appland.se/landing-pages/games-club-version-1.0/)

## Compatible Android versions

**Version 5.0 and above (95% of users)**   
*Fully supported.*  

**Version 4.x and below (5% of users)**   
*User might experience issues with the sites CSS and Javascript.*  


# Product Managers

**Action points**

1.  Update the design to your graphical profile
2.  Replace the people in the comments section with localized ones.
3.  Implement the "Enter phone number".

# Developers

You have two choices when you work with this project:

1.  Work in the framework which you find in the folder /src containing Mustache, Stylus and Javascript files. Your code is compiled with node. **This will give you the most flexibility, making it easier to maintain the project and lets you reuse a lot of code**.
2.  Work with the pre-compiled files in the folder /output containing HTML, CSS and Javascript. This will make it easy to get started but is really not recommended unless you are very unfamiliar with Mustache and Stylus.

## File structure

*   _/build-system_  
    Contains the build system that compiles the Mustache, Stylus and Javascript files. Normally you don't need to change these files.
*   _/node_modules_  
    Will be created when you run `npm install` and contain all files needed for compiling the project with node. You never need to change them.
*   _/output_  
    This is the folder where the compiled files end up after you run `npm run appland-compile`. The content in this folder is what you will publish. Read more under heading Publishing.
*   _/src_  
    This folder contains all the source files for Mustache, Stylus and Javascript. All the folders except `/src/wg` and `/src/templates` are the placeholders for the corresponding url. E.g. `/src/games` corresponds to your url http://www.your-domain.com/games and the file `index.mustache` will be the one displayed. `/src/wg` and `/src/templates` contains all the widgets that the pages are using which could be reusable objects as for example the `/src/wg/spinner-widget`.
*   _configuration.json_  
    This is the configuration file where you specify the details for your club. There are comments about each configuration but please ask us for help if you have any questions.

## Configure project
1. Setup your page to store.  
   Open the file "configuration.json" and change the value of "feed-api-store" to the store name supplied by Appland.

2. Setup your lists under "landing-page" and "games-page".  
   Login at [https://developer.applandinc.com/](https://developer.applandinc.com/) as Store manager and navigate to "Featured Zones". Here you can find the lists and the list IDs to use in the file "configuration.json".


## Build the project

1.  Install Node JS and NPM on you computer, [nodejs.org](https://nodejs.org/en/)
2.  Open a command line interface and go to the project directory (the directory this file is placed in)
3.  Run `npm install` for installing all required dependency.
4.  Run `npm start` to compile the project once and run a local server for the project. Open the url displayed in the console in a browser.

> Hint: To just compile the code you can run `npm run appland-compile`.

## Publishing

When you are publishing the project you have two alternatives:

1.  Place the content in the folder `/output` on a web server. Recommended for most of the partners
2.  Place the `/src` folder on your node server.

### Support

tobias.ekblom@appland.se
