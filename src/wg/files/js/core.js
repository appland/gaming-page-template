/**
 * Send Tracking event to Google Analytics and Appland Analytics.
 * @param category Group
 * @param name Name
 * @param label Human readable label that describe the event.
 */
function sendTrackingData(category, name, label) {
	var configuration = JSON.parse(jQuery('meta[name=configuration]').attr('content'));
	configuration['google-analytics']['data'].forEach(function (tracker) {
		ga(tracker.name + '.send', 'event', category, name, label);
	});

	var encode = encodeURIComponent || escape;
	TSA.push(['raw', 'event', 'type=' + encode(category) + '&name=' + encode(name)]); /*TSA is the Appland Tracking Service that needs to be in place to track the data in correct way. Do not remove.*/
}

function applandOnClickInstall(appId, packageName) {
	var configuration = JSON.parse(jQuery('meta[name=configuration]').attr("content"));
	// Add slash to the end of path of not preset and handle adding query parameter
	window.location.href = configuration.path + (configuration.path.endsWith("/") ? "" : "/") + (configuration.path.indexOf('?') === -1 ? '?id=' + appId : '&id=' + appId);
}

/**
 * Read a query paramter by it name.
 * @param name
 * @param url
 * @return {*}
 */
function getParameterByName(name, url) {
	if (!url) url = window.location.href;
	name = name.replace(/[\[\]]/g, "\\$&");
	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		results = regex.exec(url);
	if (!results) return null;
	if (!results[2]) return '';
	return decodeURIComponent(results[2].replace(/\+/g, " "));
}

/**
 * Fetch the given feed API URL from the cache or from remote.
 * @param feedUrl
 * @return {*}
 */
function fetchUrl(feedUrl) {
	var prefix = "response-";
	return jQuery.when(window.localStorage.getItem(prefix + feedUrl)).then(function (cachedResult) {
		var jsonResult = JSON.parse(cachedResult);
		if (jsonResult && (jsonResult.timestamp + 3600 * 100) > (new Date()).getTime()) {
			return jsonResult.data;
		} else {
			throw new Error("Invalid cache");
		}
	}).catch(function () {
		return jQuery.ajax({
			url: feedUrl,
			dataType: "json"
		}).then(function (data) {
			window.localStorage.setItem(prefix + feedUrl, JSON.stringify({
				data: data,
				timestamp: (new Date()).getTime()
			}));
			return data;
		}).fail(function (response) {
			sendTrackingData("Error", "NotAbleToShowApps", response.statusText + "; " + response.status + "; " + feedUrl);
		});
	});
}

/**
 * Check the cache after an app with the given app ID.
 * @param appId
 * @return {*}
 */
function checkCacheForAppByAppId(appId) {
	return jQuery.when(appId).then(function (appId) {
		for (var key in window.localStorage) {
			if (key.indexOf("response-") === 0) {
				var jsonResult = JSON.parse(window.localStorage.getItem(key));
				if (jsonResult && (jsonResult.timestamp + 3600 * 100) > (new Date()).getTime()) {
					jsonResult.data.tile = jsonResult.data.tile.filter(function (app) {
						return app.appId === parseInt(appId);
					});
					if (jsonResult.data.tile.length > 0) {
						return jsonResult.data;
					}
				}
			}
		}
		throw new Error("No cache found");
	});
}

/**
 * Format the result from appCatalogV4.
 * @param data
 * @return {*}
 */
function formatFeedResponse(data) {
	var configuration = JSON.parse(jQuery('meta[name=configuration]').attr("content"));
	data.configuration = configuration;
	data.path = configuration.path;
	data.tile = data.tile.filter(function (item) {
		return item.description !== undefined;
	}).map(function (item) {
		var language = Object.keys(item.description)[0];
		var benefits = item.subscriptionBenefits[0];
		return {
			description: item.description[language],
			filesize: (item.filesize / (1024 * 1024)).toFixed(2),
			isAdFree: benefits === 'ADFREE',
			hasFreeInApp: benefits === 'FREE_IN_APP_PURCHASES',
			isPremium: benefits === 'PREMIUM',
			starRating: Math.floor(item.rating / 10),
			rating: parseFloat(item.rating / 10).toFixed(1),
			ratingInPercentage: parseInt(item.rating * 2) + "%",
			imageUri: item.imageUri,
			appId: item.appId,
			company: item.company,
			supportEmail: item.support.email,
			supportUri: item.support.uri,
			version: item.version,
			ageRating: item.ageRating,
			orgPrice: item.orgPriceDisplayString,
		};
	});

	return data;
}

/**
 * Build the URI to feed API appCatalogV4 with a custom options string
 * @param options
 * @return {string}
 */
function buildFeedAPIUri(options) {
	var configuration = JSON.parse(jQuery('meta[name=configuration]').attr("content"));
	return "https://feed.appland.se/api/feed/c/appCatalogV5/store/" +
		configuration["feed-api-store"] + "/" + options + "/a/" +
		configuration["feed-api-key"] + "/" + configuration["feed-api-secret"];
}

@import "/wg/games-slider-widget/games-slider-widget.js"
@import "/wg/games-page-widget/js/main.js"
@import "/wg/game-detail-page-widget/js/main.js"
@import "/wg/testimonial-widget/testimonial-widget.js"
@import "/wg/promo-slider-widget/promo-slider-widget.js"
@import "/wg/signup-widget/signup.js"
@import "/wg/games-club-faq-page-widget/js/faq-widget.js"