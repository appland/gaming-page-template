jQuery(function ($) {
	$('.game-detail-page-widget-container').each(function (idx, el) {
		$el = $(this);
		$('body').on('click', ".sidebar-box .button", function (e) {
			e.preventDefault();
			$(this).parent().hide();
			$('.game-detail-page-widget.sidebar-box').css("max-height", "none");
			return false;
		});

		checkCacheForAppByAppId(getParameterByName("id")).catch(function () {
			return fetchUrl(buildFeedAPIUri("id/" + getParameterByName("id") + "/lang/" + $('html').attr('lang')));
		}).then(function (data) {

			var content = (formatFeedResponse(data).tile || [])[0];
			if (content) {
				var configuration = JSON.parse(jQuery('meta[name=configuration]').attr("content"));
				content.configuration = configuration;
				content.path = configuration.path;
				$el.html(Handlebars.templates["wg/game-detail-page-widget/details-page"](content));
				$('.game-detail-page-widget.screenshots-slider').owlCarousel({items: 1, autoWidth: true});
				if ($('.game-detail-page-widget.sidebar-box .app-description-text').height() <= $('.game-detail-page-widget.sidebar-box').height()) {
					$('.game-detail-page-widget.sidebar-box').css("max-height", "none");
					$(".sidebar-box .button").parent().hide();
				}
			} else {
				$el.html(Handlebars.templates["wg/game-detail-page-widget/app-not-found"](content));
			}
		}).catch(function (err) {
			$("#game").html(Handlebars.templates["wg/game-detail-page-widget/js/details-page"]({}));
		});
	});
});
