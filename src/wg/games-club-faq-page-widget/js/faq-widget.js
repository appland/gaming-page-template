/**
 * Created by vaibhav on 30/5/18
 */
var acc = document.getElementsByClassName("accordion");

function init(){
	for (let i = 0; i < acc.length; i++) {
		acc[i].addEventListener("click", accordionToggle);
	}
}

function accordionToggle() {
	for (let i = 0; i < acc.length; i++) {
		if (acc.item(i) !== this) {
			acc[i].classList.remove("active");
			acc[i].nextElementSibling.style.maxHeight = null;
		}
	}

	if (this.className.indexOf('active') !== -1) {
		this.classList.remove("active");
		this.nextElementSibling.style.maxHeight = null;
	}
	else {
		this.classList.add("active");
		const panel = this.nextElementSibling;
		panel.style.maxHeight = panel.scrollHeight + 'px';
	}

}

window.addEventListener('load', init);