jQuery(function ($) {
	function isValidMsisdn(msisdn) {
		return msisdn && msisdn.length > 4 && msisdn.length < 20;
	}

	function sendPinToMsisdn(msisdn, callback) {
		var configuration = JSON.parse(jQuery('meta[name=configuration]').attr("content"));
		// You shall send your request to your end point via POST not GET as in the example
		$.ajax({
			//type: "POST",
			type: "GET",
			url: configuration.path + "/api/send-pin.json",
			//data: JSON.stringify({
			//	msisdn: msisdn
			//}),
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			success: function (data) {
				if (data.success) {
					callback(null, data.success)
				} else {
					callback("missing token", null)
				}
			},
			failure: function (errMsg) {
				callback(errorMsg, null);
				console.log(errMsg);
			}
		});
	}

	function isValidPIN(pin) {
		return pin && pin.length === 4;
	}

	function startSubscription(msisdn, pincode, callback) {
		var configuration = JSON.parse(jQuery('meta[name=configuration]').attr("content"));
		// You shall send your request to your end point via POST not GET as in the example
		$.ajax({
			//type: "POST",
			type: "GET",
			url: configuration.path + "/api/start-subscription.json",
			//data: JSON.stringify({
			//	msisdn: msisdn,
			//	pincode: pincode
			//}),
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			success: function (data) {
				if (data.success) {
					callback(null, data)
				} else {
					callback("invalid-result", null)
				}
			},
			failure: function (errMsg) {
				callback(errMsg, null);
			}
		});
	}

	function showInputPinPopUp(msisdn) {
		var $view = $(Handlebars.templates["wg/signup-widget/signup"]({msisdn: msisdn}));
		var $infoMsg = $(".signup-widget-message-box.info", $view);
		var $errorMsg = $(".signup-widget-message-box.error", $view);

		$view.click(function (event) {
			if (event.target === event.currentTarget) {
				$view.remove();
			}
		});
		$('.close', $view).click(function () {
			$view.remove();
		});

		$('body').append($view);

		// Remove message when user start to input new code.
		$('#input-text-pin', $view).keyup(function (event) {
			$infoMsg.hide();
			$errorMsg.hide();
		});

		$('div.signup-widget-resend-code', $view).click(function (event) {
			if($(event.currentTarget).hasClass('disabled')) {
				return;
			}
			$(event.currentTarget).addClass("disabled");
			sendPinToMsisdn(msisdn, function (error, success) {
				$(event.currentTarget).removeClass("disabled");
				$errorMsg.hide();
				if(success) {
					sendTrackingData('SubscriptionPINCode', 'ResendPINSuccess', 'Resend PIN code');
					$infoMsg.html("A new PIN was sent to " + msisdn).show();
				} else {
					sendTrackingData('SubscriptionPINCode', 'ResendPINFailure', 'Failed to resend PIN Code');
					$infoMsg.html("Could not send PIN to " + msisdn).show();
				}


			});
		});

		$('.signup-widget-submit', $view).click(function (event) {
			if($(event.currentTarget).hasClass('disabled')) {
				return;
			}
			var pin = $('#input-text-pin', $view).val();
			$infoMsg.hide();
			$errorMsg.hide();
			if (!isValidPIN(pin)) {
				sendTrackingData('SubscriptionPINCode', 'SignupEnterPINFailure', 'Invalid PIN Code');
				$errorMsg.show();
			} else {
				$(event.currentTarget).addClass("disabled");
				startSubscription(msisdn, pin, function (err, data) {
					$(event.currentTarget).removeClass("disabled");
					if(data.success) {
						sendTrackingData('SubscriptionPINCode', 'SignupEnterPINSuccess', 'Signed up to Club');
						/*TSA is the Appland Tracking Service that needs to be in place to track the data in correct way. Please do not remove.*/
						TSA.push(['get-user', function(applandTrackingId) {
							var configuration = JSON.parse(jQuery('meta[name=configuration]').attr("content"));
							var encode = encodeURIComponent || escape;
							alert("DEMO NOTES: Subscription success. You will now be redirected to download the Club App. To be able to install games in the Club App you need to implement Appland Subscription API.");
							window.location.href = configuration["download-apk-domain"] + "/api/session/event/verify-subscription-widget/" + configuration["language"] + "?token=" + encode(data.token) + "&atid=" + encode(applandTrackingId);
						}]);
					} else {
						sendTrackingData('SubscriptionPINCode', 'SignupEnterPINFailure', 'Invalid PIN Code');
					}
				});
			}
		});
	}

	$('.signup-widget-input-field').keydown(function (event) {
		if (event.which === 13) { // ENTER
			$(event.currentTarget).siblings(".signup-widget-submit").click();
		}
	}).keyup(function (event) {
		$(".signup-widget-message-box").hide();
	});

	$('.signup-widget-submit').click(function (event) {
		if($(event.currentTarget).hasClass('disabled')) {
			return;
		}
		var $errorMsg = $(event.currentTarget).siblings(".signup-widget-message-box");
		var msisdn = $(event.currentTarget).siblings(".signup-widget-input-field").val();
		if (isValidMsisdn(msisdn)) {
			$(event.currentTarget).addClass("disabled");
			$errorMsg.hide();

			sendPinToMsisdn(msisdn, function (error, success) {
				if (success) {
					sendTrackingData('SubscriptionPINCode', 'EnterPhoneNumberSuccess', 'Entered Phone number');
					showInputPinPopUp(msisdn);
				} else {
					sendTrackingData('SubscriptionPINCode', 'EnterPhoneNumberFailure', 'Invalid Phone number');
					$errorMsg.show();
				}
				$(event.currentTarget).removeClass("disabled");
			});
		} else {
			sendTrackingData('SubscriptionPINCode', 'EnterPhoneNumberFailure', 'Invalid Phone number');
			$errorMsg.show();
		}
	})

});
