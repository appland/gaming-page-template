jQuery(function ($) {
	$(".games-slider-container").each(function (idx, el) {
		var $el = $(this);
		fetchUrl(buildFeedAPIUri($el.data('options'))).then(
			function (data) {
				$(".games-slider-widget-content", $el).html(Handlebars.templates["wg/games-slider-widget/js/games-slider"](formatFeedResponse(data)));
				$('.games-slider', $el).owlCarousel({
					items    : 1,
					loop     : false,
					rtl      : document.dir === 'rtl',
					autoWidth: true,
					dots     : false
				});
			}
		);
	});
});