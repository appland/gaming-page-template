jQuery(function ($) {
	$(".testimonial-widget.comments").owlCarousel({
		items: 1,
		loop: true,
		center: false,
		dots: true,
		rtl: document.dir === 'rtl',
		autoWidth: true,
		autoplay: true,

	});
});
