jQuery(function ($) {
	$(".promo-slider-widget-container").each(function (idx, el) {
		var $el = $(this);
		fetchUrl(buildFeedAPIUri($el.data('options'))).then(function (data) {
			$el.html(Handlebars.templates["wg/promo-slider-widget/promo-slider-widget"](formatFeedResponse(data)));
			$('.owl-carousel', $el).owlCarousel({
				items: 1,
				center: false,
				loop: false,
				rtl: document.dir == 'rtl',
				autoWidth: true,
				autoplay: false,
				responsiveClass:true,
				responsive : {
					960 : {
						loop: true,
						center: true,
						autoplay: true,
					}
				}
			});
		});
	})
});
