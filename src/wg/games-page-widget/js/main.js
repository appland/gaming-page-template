jQuery(function ($) {
	$(".banner-slider").each(function (idx, el) {
		var $el = $(this);
		fetchUrl(buildFeedAPIUri($el.data('options'))).then(function (data) {
			$el.html(Handlebars.templates["wg/games-page-widget/js/banner"](formatFeedResponse(data)));
			$(".owl-carousel", $el).owlCarousel({
				items: 1,
				center: true,
				loop: true,
				rtl: document.dir == 'rtl',
				autoWidth: false,
				dots: false,
				autoplay: true,
				responsive: {
					480: {
						items: 2,
						autoWidth: true
					}
				}
			});
		});
	});
});
