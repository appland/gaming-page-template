'use strict';

process.on('uncaughtException', function (err) {
	console.error(err);
	console.error(err.stack);
})

var srcName = process.argv[2] || "src"
try {
		console.log(require.resolve("app-module-path"));
} catch(e) {
		console.error("You have to run 'npm install' first...");
		process.exit(e.code);
}

require('app-module-path').addPath(__dirname + '/../');

var loadConfiguration = function () {
	var fs = require('fs');
	var path = require('path');
	var stripJsonComments = require('strip-json-comments');
	var configuration = JSON.parse(stripJsonComments(fs.readFileSync(path.join(__dirname, '..', 'configuration.json')).toString()));

	var jsonConfiguration = JSON.stringify(configuration);
	configuration.configuration = jsonConfiguration;
	return configuration;
}

var path = require('path');
var handlebars = require('handlebars');
var stylus = require('stylus');
var configuration = loadConfiguration();
var fs = require('fs');
var watch = require('node-watch');
var file = require('file');
var mkdirp = require('mkdirp');


// Compile a Mustache file to HTML
var compileMustacheFile = function (src, dest) {
	console.log("Compile " + src + " to " + dest);
	var content = fs.readFileSync(src, 'utf8')
	handlebars.registerHelper( 'str', function(args) {
		var args = Array.prototype.slice.call(arguments);
		args.pop()
		return args.join("");
	});

	handlebars.VM.resolvePartial = function (partial, context, options) {
		if(options.name.indexOf("@root/") === 0) {
			var subsrc = path.resolve(path.join(__dirname , '..', srcName , options.name.substring(6) + ".mustache"));
		} else {
			var subsrc = path.join(path.dirname(src) , options.name + ".mustache");
		}
		if(fs.existsSync(subsrc)) {
			return fs.readFileSync(subsrc, 'utf8');
		}
		throw Error("Can't find any file with name: " + subsrc);
	}
	return fs.writeFileSync(dest, handlebars.compile(content)(configuration));
}
// Compile a Mustache file to Javascript
var compileHandelbarsTemplateFile = function (src, name, dest) {
	console.log("Compile " + src + " to " + (dest || "<inline-javascript>"));
	var content = fs.readFileSync(src, 'utf8')
	var id = name.replace(/\.hbs$/gi, "").replace(/\\/g, "/");

	handlebars.registerHelper( 'str', function(args) {
		var args = Array.prototype.slice.call(arguments);
		args.pop()
		return args.join("");
	});

	var output = 'Handlebars.templates = Handlebars.templates || {}; Handlebars.templates["' + id + '"] = Handlebars.template(' + handlebars.precompile(content) + ')';
	if(dest) {
		return fs.writeFileSync(dest, output);
	} else {
		return output;
	}
}


// Compile a Stylus file to CSS
var compileStylusFile = function (src, dest) {
	console.log("Compile " + src + " to " + dest);
	var content = fs.readFileSync(src, 'utf8')

	var config = "";
	var stylusConfig = configuration.stylus || {};
	stylusConfig.path = '"' + configuration.path + '"';
	for(var key in stylusConfig) {
		config += key + " = " + stylusConfig[key] + "\n";
	}

	var funcCode = fs.readFileSync(path.join(__dirname, 'styl.styl')) + "\n";
	try {
		return fs.writeFileSync(dest, stylus(funcCode + config + content).set('paths', [__dirname + '/../' + srcName]).render());
	} catch(err) {
		console.error("Error in " + src);
		console.error(err.message)
	}
}


// Compile a Stylus file to CSS
var compileJavascriptFile = function (src, dest) {
	console.log("Compile " + src + " to " + dest);
	var resolveFile = function(src) {
		var content = fs.readFileSync(src, 'utf8');
		if (content == undefined) {
				throw new Error("Failed to read file " + src);
		}
		return content.replace(/\@import[\t\ ]+(?:\"|\')(.*?)(?:\"|\')/gi, function(str, name) {
			var subsrc = undefined;
			if(name.indexOf("/") === 0) {
				subsrc = path.join(__dirname,	'..' , srcName, name);
			} else {
				subsrc = path.join(path.dirname(src),	name);
			}
			String.prototype.endsWith = function(suffix) {
			    return this.indexOf(suffix, this.length - suffix.length) !== -1;
			};
			if(subsrc.endsWith(".hbs")) {
				return compileHandelbarsTemplateFile(subsrc, path.relative(__dirname + '/../' + srcName, subsrc))
			} else {
				return resolveFile(subsrc);
			}
		});
	}

	fs.writeFileSync(dest, resolveFile(src)); // Write the result.
}


// Copy a file.
var copyFile = function (src, dest) {
	console.log("Copy " + src + " to " + dest);
	fs.createReadStream(src).pipe(fs.createWriteStream(dest))
}

// Resolve the destionation path from the src path.
var resolveDestionationPath = function (file, fileExtension) {
	var relativeFile = path.relative(__dirname + '/../' + srcName, file);
	var destFilePath = path.join(__dirname, '/../output/', relativeFile);

	mkdirp.sync(path.dirname(destFilePath));
	if (fileExtension) {
		return path.join(path.dirname(destFilePath), path.basename(destFilePath, path.extname(destFilePath)) + "." + fileExtension);
	} else {
		return destFilePath;
	}
}

// When a file has be change and needs to be updated.
var onFileChange = function (file) {
	if (file.match(/\.mustache$/i)) {
		compileMustacheFile(file, resolveDestionationPath(file, "html"));
	} else if (file.match(/\.hbs$/i)) {
		// This line is disable since you should include you hbs files into a javascript file for use them.
		// compileHandelbarsTemplateFile(file, path.relative(__dirname + '/../' + srcName, file), resolveDestionationPath(file, "js"));
	} else if (file.match(/\.js$/i)) {
		compileJavascriptFile(file, resolveDestionationPath(file, "js"));
	} else if (file.match(/\.styl$/i)) {
		compileStylusFile(file, resolveDestionationPath(file, "css"));
	} else {
		copyFile(file, resolveDestionationPath(file));
	}
}

// Scan all files that match the regexp and invoke onFileChange.
var scanFiles = function (regexp) {
	file.walk(__dirname + '/../' + srcName, function (err, src, paths, files) {
		for (var idx in files) {
			if (files[idx].match(regexp)) {
				onFileChange(files[idx]);
			}
		}
	})
}

// List all files.
file.walk(__dirname + '/../' + srcName, function (err, src, paths, files) {
	for (var idx in files) {
		onFileChange(files[idx]);
	}
})

// Watch for file changes.
if (process.argv[3] == '--watch' || process.argv[3] == '--server-mode') {


	watch(__dirname + '/../' + srcName, {recursive: true}, function (evt, file) {
		if (fs.existsSync(file)) {
			if (file.match(/\.mustache$/i)) {
				scanFiles(/\.mustache$/i);
			} else if (file.match(/\.styl$/i)) {
				scanFiles(/\.styl$/i);
			} else if (file.match(/\.js|\.hbs$/i)) {
				scanFiles(/\.js|\.hbs$/i);
			} else {
				onFileChange(file);
			}
		}
	});

	watch(__dirname + '/../configuration.json', {recursive: true}, function() {
		configuration = loadConfiguration();
		// List all files.
		file.walk(__dirname + '/../' + srcName, function (err, src, paths, files) {
			for (var idx in files) {
				onFileChange(files[idx]);
			}
		})
	});
}

setTimeout(function () {
	console.log(process.argv[3]);
	if(process.argv[3] == '--server-mode') {
			var nodeServer = require('node-static');
			var fileServer = new nodeServer.Server(path.join(__dirname, '/../output/'), { cache: 0 });
			require('http').createServer(function (request, response) {
				request.addListener('end', function () {
						fileServer.serve(request, response);
				}).resume();
			}).listen(8080, "0.0.0.0");
			console.log("======================");
			console.log("Open http://localhost:8080/");
	}
}, 2500);
